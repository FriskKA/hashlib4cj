package hashlib4cj

public class HashBuffer <: ToString {
    private var buffer: Array<UInt8>
    private var pos: Int = 0

    // 构造函数
    protected init(length: Int) {
        // require(length > 0, "Buffer length must be positive")
        buffer = Array<UInt8>(length, repeat: 0)
        initialize()
    }

    // 重置缓冲区指针
    public func initialize(): Unit {
        pos = 0
    }

    // 获取完整缓冲区数据（自动重置）
    public func getBytes(): Array<UInt8> {
        //debug.assert(isFull, "Buffer not full")
        pos = 0
        return buffer.clone() // 返回副本保证数据安全
    }

    // 获取零填充的缓冲区数据
    public func getBytesZeroPadded(): Array<UInt8> {
        let zeros = buffer.size - pos
        if (zeros > 0) {
            buffer[pos..buffer.size].fill(0)
        }
        pos = 0
        return buffer
    }

    @OverflowWrapping
    public func feed(
        data: Array<UInt8>,
        startIndex: Int,
        length: Int,
        processed: UInt64
    ): (Bool, Int, Int, UInt64) {
        // debug.assert(startIndex >= 0 && length >= 0, "Invalid index/length")
        // debug.assert(startIndex + length <= data.size, "Index out of range")
        // debug.assert(!isFull, "Buffer already full")

        if (data.isEmpty() || length == 0) {
            return (false, 0, 0, 0)
        }

        let copyLen = min(buffer.size - pos, length)
        data.copyTo(
            buffer,
            startIndex,
            pos,
            copyLen
        )
        pos += copyLen
        // startIndex += copyLen
        // length -= copyLen
        // processed += copyLen.toUInt64()
        return (isFull, startIndex + copyLen, length - copyLen, processed + UInt64(copyLen))
    }

    @OverflowWrapping
    public func feed(data: Array<UInt8>, length: Int): Bool {
        // debug.assert(length >= 0 && length <= data.size, "Invalid length")
        // debug.assert(!isFull, "Buffer already full")

        if (data.isEmpty() || length == 0) {
            return false
        }

        let copyLen = min(buffer.size - pos, length)
        data.copyTo(buffer, 0, pos, copyLen)
        pos += copyLen
        return isFull
    }

    // 属性访问器
    public prop isEmpty: Bool {
        get() {
            pos == 0
        }
    }
    public prop position: Int {
        get() {
            pos
        }
    }
    public prop capacity: Int {
        get() {
            buffer.size
        }
    }
    public prop isFull: Bool {
        get() {
            pos == buffer.size
        }
    }

    // 调试信息
    public override func toString(): String {
        return "HashBuffer[capacity=${capacity}, pos=${pos}, empty=${isEmpty}]"
    }
}
