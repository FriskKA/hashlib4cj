package hashlib4cj.toolkit

foreign func memcpy(dest: CPointer<Unit>, src: CPointer<Unit>, count: UIntNative): CPointer<Byte>

public func blockCopy<T, U>(srcArray: Array<T>, srcOffset: Int, dstArray: Array<U>, dstOffset: Int, count: Int) where T <: CType,
    U <: CType {
    if (srcArray.size <= 0) {
        throw IllegalArgumentException("Source array is empty")
    }

    if (dstArray.size <= 0) {
        throw IllegalArgumentException("Destination array is empty")
    }

    if (count < 0) {
        throw IllegalArgumentException("Copy count cannot be negative")
    }

    let srcTypeSize = Int64(sizeOf<T>())
    let dstTypeSize = Int64(sizeOf<U>())

    // 计算源数组的总字节数
    let srcTotalBytes = srcArray.size * srcTypeSize
    // 计算目标数组的总字节数
    let dstTotalBytes = dstArray.size * dstTypeSize

    // 检查源偏移量是否超出范围
    if (srcOffset < 0 || srcOffset >= srcTotalBytes) {
        throw IllegalArgumentException("Source offset is out of bounds")
    }

    // 检查目标偏移量是否超出范围
    if (dstOffset < 0 || dstOffset >= dstTotalBytes) {
        throw IllegalArgumentException("Destination offset is out of bounds")
    }

    // 计算所需的源元素数量
    let requiredSrcBytes = srcOffset + count
    if (requiredSrcBytes > srcTotalBytes) {
        throw IllegalArgumentException("Invalid copy length for source array")
    }

    // 计算所需的目标元素数量
    let requiredDstBytes = dstOffset + count
    if (requiredDstBytes > dstTotalBytes) {
        throw IllegalArgumentException("Invalid copy length for destination array")
    }

    unsafe {
        let srcHandle = acquireArrayRawData(srcArray)
        let dstHandle = acquireArrayRawData(dstArray)
        let sourcePointer = CPointer<Unit>(srcHandle.pointer) + srcOffset
        let destinationPointer = CPointer<Unit>(dstHandle.pointer) + dstOffset
        memcpy(destinationPointer, sourcePointer, UIntNative(count))
        releaseArrayRawData(CPointerHandle<Unit>(sourcePointer))
        releaseArrayRawData(CPointerHandle<Unit>(destinationPointer))
    }
}
