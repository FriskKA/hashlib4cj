package hashlib4cj

import std.reflect.*
import hashlib4cj.toolkit.*
import std.io.*
import std.console.*
import std.fs.*
import std.sync.*
import std.collection.concurrent.*
import hashlib4cj.extensions.*

internal abstract class Hash <: IHash {
    public static var BUFFER_SIZE: Int64 = 64 * 1024

    public Hash(private let _hashSize: Int64, private let _blockSize: Int64) {}

    public prop name: String {
        get() {
            TypeInfo.of(this).name
        }
    }

    public prop hashSize: Int64 {
        get() {
            _hashSize
        }
    }

    public prop blockSize: Int64 {
        get() {
            _blockSize
        }
    }

    public func initialize(): Unit

    public func transformBytes(data: Array<UInt8>, index: Int64, length: Int64): Unit

    public func transformFinal(): HashResult

    public func computeAny(inputData: Any): HashResult {
        return match (inputData) {
            case value: Byte => computeByte(value)
            case value: Rune => computeRune(value)
            case value: String => computeStringUTF8(value)

            case value: Int8 => computeInt8(value)
            case value: Int16 => computeInt16(value)
            case value: Int32 => computeInt32(value)
            case value: Int64 => computeInt64(value)

            case value: UInt16 => computeUInt16(value)
            case value: UInt32 => computeUInt32(value)
            case value: UInt64 => computeUInt64(value)

            case value: Float32 => computeFloat32(value)
            case value: Float64 => computeFloat64(value)

            case value: Array<Rune> => computeRunes(value)
            case value: Array<UInt8> => computeBytes(value)
            case value: Array<Int16> => computeInt16s(value)
            case value: Array<Int32> => computeInt32s(value)
            case value: Array<Int64> => computeInt64s(value)
            case value: Array<UInt16> => computeUInt16s(value)
            case value: Array<UInt32> => computeUInt32s(value)
            case value: Array<UInt64> => computeUInt64s(value)

            case value: Array<Float32> => computeFloat32s(value)
            case value: Array<Float64> => computeFloat64s(value)

            case _ => throw Exception("unsupport data type ${TypeInfo.of(inputData)}")
        }
    }
    public func computeInt8(data: Int8): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeByte(data: Byte): HashResult {
        let temp: Array<Byte> = [data]
        return computeBytes(temp)
    }

    public func computeRune(data: Rune): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeInt16(data: Int16): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeUInt16(data: UInt16): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeInt32(data: Int32): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeUInt32(data: UInt32): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeInt64(data: Int64): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeUInt64(data: UInt64): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeFloat32(data: Float32): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeFloat64(data: Float64): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeBool(data: Bool): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeStringUTF8(data: String): HashResult {
        computeBytes(getBytes(data))
    }

    public func computeRunes(data: Array<Rune>): HashResult {
        computeBytes(convertRunesToBytes(data))
    }
    // public func computeBytes(data: Array<Byte>): HashResult {
    //     initialize()
    //     transformBytes(data)
    //     let result = transformFinal()
    //     initialize()
    //     result
    // }

    public func computeInt16s(data: Array<Int16>): HashResult {
        computeBytes(convertInt16sToBytes(data))
    }

    public func computeUInt16s(data: Array<UInt16>): HashResult {
        computeBytes(convertUInt16sToBytes(data))
    }

    public func computeInt32s(data: Array<Int32>): HashResult {
        computeBytes(convertInt32sToBytes(data))
    }

    public func computeUInt32s(data: Array<UInt32>): HashResult {
        computeBytes(convertUInt32sToBytes(data))
    }

    public func computeInt64s(data: Array<Int64>): HashResult {
        computeBytes(convertInt64sToBytes(data))
    }

    public func computeUInt64s(data: Array<UInt64>): HashResult {
        computeBytes(convertUInt64sToBytes(data))
    }

    // public func computeFloat16s(data: Array<Float16>): HashResult { 
    //     computeBytes(convertFloat16sToBytes(data))
    // }

    public func computeFloat32s(data: Array<Float32>): HashResult {
        computeBytes(convertFloat32sToBytes(data))
    }

    public func computeFloat64s(data: Array<Float64>): HashResult {
        computeBytes(convertFloat64sToBytes(data))
    }

    public func computeFile(fileName: String, from: Int64, length: Int64): HashResult {
        initialize()
        transformFile(fileName, from, length)
        let result = transformFinal()
        initialize()
        return result
    }

    public func computeFileFromPath(fileName: Path, from: Int64, length: Int64): HashResult {
        initialize();
        transformFile(fileName, from, length)
        let result = transformFinal()
        initialize();
        return result;
    }

    public open func computeBytes(data: Array<Byte>): HashResult {
        initialize()
        transformBytes(data)
        let result = transformFinal()
        initialize()
        result
    }

    public func computeStream(data: IOStream, length!: Int64 = -1): HashResult {
        initialize()
        transformStream(data, length: length)
        let result = transformFinal()
        initialize()
        result
    }

    public func transformAny(data: Any): Unit {
        match (data) {
            case value: Byte => transformByte(value)
            case value: Rune => transformRune(value)
            case value: String => transformStringUTF8(value)

            case value: Int8 => transformInt8(value)
            case value: Int16 => transformInt16(value)
            case value: Int32 => transformInt32(value)
            case value: Int64 => transformInt64(value)
            case value: UInt16 => transformUInt16(value)
            case value: UInt32 => transformUInt32(value)
            case value: UInt64 => transformUInt64(value)

            case value: Float32 => transformFloat32(value)
            case value: Float64 => transformFloat64(value)

            case value: Array<Byte> => transformBytes(value)
            case value: Array<Rune> => transformRunes(value)
            case value: Array<Int16> => transformInt16Array(value)
            case value: Array<Int32> => transformInt32s(value)
            case value: Array<Int64> => transformInt64s(value)
            case value: Array<UInt16> => transformUInt16s(value)
            case value: Array<UInt32> => transformUInt32s(value)
            case value: Array<UInt64> => transformUInt64s(value)
            case value: Array<Float32> => transformFloat32s(value)
            case value: Array<Float64> => transformFloat64s(value)

            case _ => throw Exception("unsupport data type ${TypeInfo.of(data).toString()}")
        }
    }

    public func transformBool(data: Bool): Unit {
        transformBytes(getBytes(data))
    }

    public func transformByte(data: Byte): Unit {
        let temp: Array<Byte> = [data]
        transformBytes(temp)
    }

    public func transformRune(data: Rune): Unit {
        transformBytes(getBytes(data))
    }

    public func transformInt8(data: Int8): Unit {
        transformBytes(getBytes(data))
    }

    public func transformInt16(data: Int16): Unit {
        transformBytes(getBytes(data))
    }

    public func transformUInt16(data: UInt16): Unit {
        transformBytes(getBytes(data))
    }

    public func transformInt32(data: Int32): Unit {
        transformBytes(getBytes(data))
    }

    public func transformUInt32(data: UInt32): Unit {
        transformBytes(getBytes(data))
    }

    public func transformInt64(data: Int64): Unit {
        transformBytes(getBytes(data))
    }

    public func transformUInt64(data: UInt64): Unit {
        transformBytes(getBytes(data))
    }

    public func transformFloat32(data: Float32): Unit {
        transformBytes(getBytes(data))
    }

    public func transformFloat64(data: Float64): Unit {
        transformBytes(getBytes(data))
    }

    public func transformStringUTF8(data: String): Unit {
        transformBytes(getBytes(data))
    }

    public func transformRunes(data: Array<Rune>): Unit {
        transformBytes(convertRunesToBytes(data))
    }

    public func transformInt16Array(data: Array<Int16>): Unit {
        transformBytes(convertInt16sToBytes(data))
    }
    public func transformUInt16s(data: Array<UInt16>): Unit {
        transformBytes(convertUInt16sToBytes(data))
    }

    public func transformInt32s(data: Array<Int32>): Unit {
        transformBytes(convertInt32sToBytes(data))
    }

    public func transformUInt32s(data: Array<UInt32>): Unit {
        transformBytes(convertUInt32sToBytes(data))
    }

    public func transformInt64s(data: Array<Int64>): Unit {
        transformBytes(convertInt64sToBytes(data))
    }

    public func transformUInt64s(data: Array<UInt64>): Unit {
        transformBytes(convertUInt64sToBytes(data))
    }

    public func transformFloat32s(data: Array<Float32>): Unit {
        transformBytes(convertFloat32sToBytes(data))
    }

    public func transformFloat64s(data: Array<Float64>): Unit {
        transformBytes(convertFloat64sToBytes(data))
    }

    public func transformFile(fileName: String, from: Int64, length: Int64): Unit {
        // TODO Debug.Assert
        try (file = File(fileName, Read)) {
            file.seek(SeekPosition.Begin(from))
            transformStream(file, length: length)
        }
    }
    public func transformFile(fileName: Path, from: Int64, length: Int64): Unit {
        // TODO Debug.Assert
        try (file = File(fileName, Read)) {
            file.seek(SeekPosition.Begin(from))
            transformStream(file, length: length)
        }
    }

    public func transformStream(dataStream: IOStream, length!: Int64 = -1): Unit {
        let typeInfo = TypeInfo.of(dataStream)
        if (typeInfo.isSubtypeOf(TypeInfo.of<Seekable>())) {
            let seekable = (dataStream as Seekable).getOrThrow()
            if (length >= 0 && seekable.position + length > seekable.length) {
                throw Exception("The length is greater than the file size")
            }
            if (seekable.position >= seekable.length) {
                return
            }
        }
        let queue = BlockingQueue<Array<Byte>>()
        let readerSpawn = spawn {
            var total: Int64 = 0
            while (true) {
                var data = Array<Byte>(BUFFER_SIZE, repeat: 0)
                let readed = dataStream.read(data)
                if (readed == 0) {
                    break
                }

                let chunk: Array<Byte>
                if (length == -1) {
                    chunk = data[0..readed]
                } else {
                    let remaining = length - total
                    let cut = min(readed, remaining)
                    chunk = data[0..cut]
                    total += cut
                    if (total >= length) {
                        break
                    }
                }
                queue.enqueue(chunk)
            }
        }
        let hasherSpawn = spawn {
            let hash: Hash = this
            var total: Int64 = 0
            while (true) {
                match (queue.dequeue(Duration.second)) {
                    case Some(data) =>
                        total += data.size
                        if (length == -1 || total < length) {
                            hash.transformBytes(data, 0, data.size)
                        } else {
                            var readed = data.size
                            readed = readed - (total - length)
                            hash.transformBytes(data, 0, readed)
                        }
                        match {
                            case length == -1 && data.size != BUFFER_SIZE => break
                            case total == length => break
                            case data.size != BUFFER_SIZE => throw Exception("EndofStreamException")
                            case _ => ()
                        }
                    case None => continue
                }
            }
        }
        readerSpawn.get()
        hasherSpawn.get()
    }

    public open func transformBytes(data: Array<Byte>): Unit {
        transformBytes(data, 0, data.size)
    }

    public open func transformBytes(data: Array<Byte>, index: Int64): Unit {
        if (index < 0) {
            throw Exception("The data can not be empty!index: ${index}")
        }
        let length = data.size - index
        if (length < 0) {
            throw Exception("The data can not be empty!length: ${length}")
        }
        transformBytes(data, index, length)
    }
}
