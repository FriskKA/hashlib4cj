package hashlib4cj.test.toolkit_test

import std.unittest.*
import std.unittest.testmacro.*
import std.binary.*
import hashlib4cj.toolkit.*

@Test
public class BitConverterTest {
    @TestCase
    public func testBoolBitConverter_True(): Unit {
        let input: Array<Byte> = [1]
        @Expect(getBytes(true), input)
    }

    @TestCase
    public func testBoolBitConverter_False(): Unit {
        let input: Array<Byte> = [0]
        @Expect(getBytes(false), input)
    }

    @TestCase
    public func testRuneBitConverter(): Unit {
        let testData: Rune = '\u{0041}' // Unicode for 'A'
        let input: Array<Byte> = [65, 0, 0, 0]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testStringBitConverter(): Unit {
        let testData: String = "Hello"
        let input: Array<Byte> = [72, 101, 108, 108, 111]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testInt8BitConverter(): Unit {
        let testData: Int8 = -64
        let input: Array<Byte> = [192]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testInt16BitConverter(): Unit {
        let testData: Int16 = -64
        let input: Array<Byte> = [192, 255]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testInt32BitConverter(): Unit {
        let testData: Int32 = -64
        let input: Array<Byte> = [192, 255, 255, 255]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testInt64BitConverter(): Unit {
        let testData: Int64 = -64
        let input: Array<Byte> = [192, 255, 255, 255, 255, 255, 255, 255]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testUInt8BitConverter(): Unit {
        let testData: UInt8 = 64
        let input: Array<Byte> = [64]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testUInt16BitConverter(): Unit {
        let testData: UInt16 = 64
        let input: Array<Byte> = [64, 0]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testUInt32BitConverter(): Unit {
        let testData: UInt32 = 64
        let input: Array<Byte> = [64, 0, 0, 0]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testUInt64BitConverter(): Unit {
        let testData: UInt64 = 64
        let input: Array<Byte> = [64, 0, 0, 0, 0, 0, 0, 0]
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testFloat32BitConverter(): Unit {
        let testData: Float32 = 1.23
        let input: Array<Byte> = [164, 112, 157, 63] // Little Endian representation of 1.23
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testFloat64BitConverter(): Unit {
        let testData: Float64 = 1.23
        let input: Array<Byte> = [174, 71, 225, 122, 20, 174, 243, 63] // Little Endian representation of 1.23
        @Expect(getBytes(testData), input)
    }

    @TestCase
    public func testRunesArrayBitConverter(): Unit {
        let testData: Array<Rune> = [r'A', r'B', r'C']
        let input: Array<Byte> = [65, 0, 0, 0, 66, 0, 0, 0, 67, 0, 0, 0]
        @Expect(convertRunesToBytes(testData), input)
    }

    @TestCase
    public func testInt8ArrayBitConverter(): Unit {
        let testData: Array<Int8> = [-1, -2, -3]
        let input: Array<Byte> = [255, 254, 253]
        @Expect(convertInt8sToBytes(testData), input)
    }

    @TestCase
    public func testInt16ArrayBitConverter(): Unit {
        let testData: Array<Int16> = [-1, -2, -3]
        let input: Array<Byte> = [255, 255, 254, 255, 253, 255]
        @Expect(convertInt16sToBytes(testData), input)
    }

    @TestCase
    public func testInt32ArrayBitConverter(): Unit {
        let testData: Array<Int32> = [-1, -2, -3]
        let input: Array<Byte> = [255, 255, 255, 255, 254, 255, 255, 255, 253, 255, 255, 255]
        @Expect(convertInt32sToBytes(testData), input)
    }

    @TestCase
    public func testInt64ArrayBitConverter(): Unit {
        let testData: Array<Int64> = [-1, -2, -3]
        let input: Array<Byte> = [255, 255, 255, 255, 255, 255, 255, 255, 254, 255, 255, 255, 255, 255, 255, 255, 253,
            255, 255, 255, 255, 255, 255, 255]
        @Expect(convertInt64sToBytes(testData), input)
    }

    @TestCase
    public func testUInt8ArrayBitConverter(): Unit {
        let testData: Array<UInt8> = [1, 2, 3]
        let input: Array<Byte> = [1, 2, 3]
        @Expect(convertUInt8sToBytes(testData), input)
    }

    @TestCase
    public func testUInt16ArrayBitConverter(): Unit {
        let testData: Array<UInt16> = [1, 2, 3]
        let input: Array<Byte> = [1, 0, 2, 0, 3, 0]
        @Expect(convertUInt16sToBytes(testData), input)
    }

    @TestCase
    public func testUInt32ArrayBitConverter(): Unit {
        let testData: Array<UInt32> = [1, 2, 3]
        let input: Array<Byte> = [1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0]
        @Expect(convertUInt32sToBytes(testData), input)
    }

    @TestCase
    public func testUInt64ArrayBitConverter(): Unit {
        let testData: Array<UInt64> = [1, 2, 3]
        let input: Array<Byte> = [1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0]
        @Expect(convertUInt64sToBytes(testData), input)
    }
    @TestCase
    public func testConvertBytesToUInt32_LittleEndian(): Unit {
        let testData: UInt32 = 0x12345678
        let bytes: Array<UInt8> = [0x78, 0x56, 0x34, 0x12]
        let result: UInt32 = convertBytesToUInt32(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToUInt32_LittleEndian_Boundary(): Unit {
        let testData: UInt32 = 0xFFFFFFFF
        let bytes: Array<UInt8> = [0xFF, 0xFF, 0xFF, 0xFF]
        let result: UInt32 = convertBytesToUInt32(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToUInt32_LittleEndian_Zero(): Unit {
        let testData: UInt32 = 0
        let bytes: Array<UInt8> = [0x00, 0x00, 0x00, 0x00]
        let result: UInt32 = convertBytesToUInt32(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToInt64_LittleEndian(): Unit {
        let testData: Int64 = -1
        let bytes: Array<UInt8> = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
        let result: Int64 = convertBytesToInt64(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToInt64_LittleEndian_Positive(): Unit {
        let testData: Int64 = 0x123456789ABCDEF0
        let bytes: Array<UInt8> = getBytes(testData)
        let result: Int64 = convertBytesToInt64(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToInt64_LittleEndian_Negative(): Unit {
        let testData: Int64 = -0x123456789ABCDEF0
        let bytes: Array<UInt8> = getBytes(testData)
        let result: Int64 = convertBytesToInt64(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToUInt64_LittleEndian(): Unit {
        let testData: UInt64 = 0x123456789ABCDEF0
        let bytes: Array<UInt8> = [0xF0, 0xDE, 0xBC, 0x9A, 0x78, 0x56, 0x34, 0x12]
        let result: UInt64 = convertBytesToUInt64(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToUInt64_LittleEndian_MaxValue(): Unit {
        let testData: UInt64 = 0xFFFFFFFFFFFFFFFF
        let bytes: Array<UInt8> = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
        let result: UInt64 = convertBytesToUInt64(bytes, 0)
        @Expect(result, testData)
    }

    @TestCase
    public func testConvertBytesToUInt64_LittleEndian_Zero(): Unit {
        let testData: UInt64 = 0
        let bytes: Array<UInt8> = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        let result: UInt64 = convertBytesToUInt64(bytes, 0)
        @Expect(result, testData)
    }
}
