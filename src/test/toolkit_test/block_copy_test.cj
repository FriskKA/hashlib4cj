package hashlib4cj.test.toolkit_test

import std.unittest.*
import std.unittest.testmacro.*
import hashlib4cj.toolkit.*

@Test
public class BlockCopyTest {
    @TestCase
    public func testBlockCopy_Int32_To_UInt8_Normal(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(12, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 0, 12)
        let expected: Array<UInt8> = [1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_Int32_To_UInt8_Offset(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3, 4]
        let dstArray: Array<UInt8> = Array<UInt8>(12, repeat: 0)
        blockCopy(srcArray, 1, dstArray, 0, 12)
        let expected: Array<UInt8> = [0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_Int32_To_UInt8_DstOffset(): Unit {
        let srcArray: Array<Int32> = [1, 2]
        let dstArray: Array<UInt8> = Array<UInt8>(12, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 4, 8)
        let expected: Array<UInt8> = [0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_UInt8_To_UInt8_Normal(): Unit {
        let srcArray: Array<UInt8> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(3, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 0, 3)
        let expected: Array<UInt8> = [1, 2, 3]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_UInt8_To_UInt8_Offset(): Unit {
        let srcArray: Array<UInt8> = [1, 2, 3, 4]
        let dstArray: Array<UInt8> = Array<UInt8>(3, repeat: 0)
        blockCopy(srcArray, 1, dstArray, 0, 3)
        let expected: Array<UInt8> = [2, 3, 4]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_UInt8_To_UInt8_DstOffset(): Unit {
        let srcArray: Array<UInt8> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(6, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 3, 3)
        let expected: Array<UInt8> = [0, 0, 0, 1, 2, 3]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_Float32_To_UInt8_Normal(): Unit {
        let srcArray: Array<Float32> = [1.23, 4.56]
        let dstArray: Array<UInt8> = Array<UInt8>(8, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 0, 8)
        let expected: Array<UInt8> = [164, 112, 157, 63, 133, 235, 145, 64]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_Float32_To_UInt8_Offset(): Unit {
        let srcArray: Array<Float32> = [1.23, 4.56, 7.89]
        let dstArray: Array<UInt8> = Array<UInt8>(8, repeat: 0)
        blockCopy(srcArray, 4, dstArray, 0, 8)
        let expected: Array<UInt8> = [133, 235, 145, 64, 225, 122, 252, 64]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_Float32_To_UInt8_DstOffset(): Unit {
        let srcArray: Array<Float32> = [1.23, 4.56]
        let dstArray: Array<UInt8> = Array<UInt8>(12, repeat: 0)
        blockCopy(srcArray, 0, dstArray, 4, 8)
        let expected: Array<UInt8> = [0, 0, 0, 0, 164, 112, 157, 63, 133, 235, 145, 64]
        @Expect(dstArray, expected)
    }

    @TestCase
    public func testBlockCopy_EmptySourceArray(): Unit {
        let srcArray: Array<Int32> = []
        let dstArray: Array<UInt8> = Array<UInt8>(0, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, 0, 0)
        })
    }

    @TestCase
    public func testBlockCopy_NegativeSize(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(3, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, 0, -1)
        })
    }

    @TestCase
    public func testBlockCopy_SourceOffsetOutOfBounds(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(3, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 3 * 4, dstArray, 0, 1)
        })
    }

    @TestCase
    public func testBlockCopy_NegativeDstOffset(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(3, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, -1, 1)
        })
    }

    @TestCase
    public func testBlockCopy_InvalidCopyLength(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(13, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, 0, 13)
        })
    }

    @TestCase
    public func testBlockCopy_DestinationOffsetOutOfBounds(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(16, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, 16, 1)
        })
    }

    @TestCase
    public func testBlockCopy_CopyExceedsDestinationBounds(): Unit {
        let srcArray: Array<Int32> = [1, 2, 3]
        let dstArray: Array<UInt8> = Array<UInt8>(12, repeat: 0)
        @ExpectThrows({
            blockCopy(srcArray, 0, dstArray, 12, 1)
        })
    }
}
