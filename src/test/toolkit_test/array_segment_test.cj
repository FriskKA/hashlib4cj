package hashlib4cj.test.toolkit_test

import std.unittest.*
import std.unittest.testmacro.*
import hashlib4cj.toolkit.ArraySegment
import std.collection.*

@Test
public class ArraySegmentTest {
    @TestCase
    public func testNegativeOffset(): Unit {
        let arr = [1u8, 2u8]
        @ExpectThrows({
            let _ = ArraySegment(arr, 2, 3)
        })
    }

    @TestCase
    public func testSizeOverflow(): Unit {
        let arr = [1.0, 2.0, 3.0]
        @ExpectThrows({
            let _ = ArraySegment(arr, 2, 3)
        })
    }

    // // region 索引访问测试
    @TestCase
    public func testIndexAccess(): Unit {
        let arr = [10, 20, 30, 40, 50]
        let seg = ArraySegment(arr, 1, 3)

        @Expect(seg[0], 20)
        @Expect(seg[2], 40)
    }

    @TestCase
    public func testIndexOutOfRange(): Unit {
        let seg = ArraySegment([1, 2, 3], 1, 2)

        @ExpectThrows({
            let _ = seg[-1]
        })

        @ExpectThrows({
            let _ = seg[2]
        })
    }
    // // endregion

    // // region 范围访问测试
    @TestCase
    public func testRangeAccess(): Unit {
        let arr = [0, 1, 2, 3, 4, 5]
        let seg = ArraySegment(arr, 2, 4)

        let sub = seg[1..3]
        @Expect(sub, [3, 4])
    }

    @TestCase
    public func testFullRange(): Unit {
        let seg = ArraySegment(["a", "b", "c", "d"])
        @Expect(seg[0..4], ["a", "b", "c", "d"])
    }
    // // endregion

    //迭代器测试
    @TestCase
    public func testIterator(): Unit {
        let arr = [1u8, 2u8, 3u8, 4u8, 5u8]
        let seg = ArraySegment(arr, 1, 3)

        var result = ArrayList<UInt8>()
        for (item in seg) {
            result.add(item)
        }

        @Expect(result.toArray(), [2u8, 3u8, 4u8])
    }
    //边界测试
    @TestCase
    public func testZeroSizeSegment(): Unit {
        let arr = [1, 2, 3]
        let seg = ArraySegment(arr, 2, 0)

        @Expect(seg.size, 0)
        @ExpectThrows({
            let _ = seg[0]
        })
    }

    @TestCase
    public func testMaxSizeSegment(): Unit {
        let arr = [1.1, 2.2, 3.3]
        let seg = ArraySegment(arr, 0, arr.size)
        @Expect(seg.size, arr.size)
    }

    //类型泛型测试
    @TestCase
    public func testStringType(): Unit {
        let arr = ["apple", "banana", "cherry"]
        let seg = ArraySegment(arr, 1, 2)

        @Expect(seg[0], "banana")
        @Expect(seg[1], "cherry")
    }
    
    @TestCase
    public func testCustomStructType(): Unit {
        let arr = [testStruct(1,2), testStruct(3,4)]
        let seg = ArraySegment(arr)
        @Expect(seg.size, 2)
        @Expect(seg[1].x, 3)
    }
}
public struct testStruct{
   public let x: Int
   public let y: Int
   public init(x: Int, y: Int) {
        this.x = x
        this.y = y
   }
}
