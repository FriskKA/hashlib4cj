package hashlib4cj.hash32

import hashlib4cj.*
import std.math.numeric.BigInt
import hashlib4cj.extensions

public class AP <: Hash & IHash32 & IBlockHash {
    var apHash: UInt32 = 0
    var apIndex: Int = 0

    public AP() {
        super(4, 1)
    }

    public override func initialize(): Unit {
        apHash = 0
        apIndex = 0
    }
    public override func transformFinal(): HashResult {
        return HashResult(apHash)
    }

    public override func transformBytes(bytes: Array<UInt8>, index: Int, length: Int): Unit {
        if (index < 0 || length < 0 || index + length > bytes.size || index + length < 0) {
            throw Exception("Invalid index or length")
        }
        for (i in 0..length) {
            apHash ^= match (apIndex & 1) {
                case 0 => ((apHash<<7) ^ UInt32(bytes[i]) ^ (apHash>>3))
                case 1 => !((apHash<<11) ^ UInt32(bytes[i + index]) ^ (apHash>>5))
                case _ => throw Exception("Invalid apIndex${apIndex}")
            }
            apIndex++
        }
    }
}
